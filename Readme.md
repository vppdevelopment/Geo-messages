# Instalacion de ambiente

## src (Codigo fuente)

## Examples (Ejemplos funcionando de las tecnologias a utilizar)

Geolocalizacion basada en rangos de busqueda:
http://developer.factual.com/common-places-use-case-examples/
* Algunas caracteristicas requieren cuenta premium, asi que debemos investigar cuales podemos usar

APIdocs de Factual:
http://developer.factual.com/api-docs/

Uso del API de factual para hacer busquedas por rango y ubicacion
http://developer.factual.com/working-with-factual-places/index.html

Clasificacion de lugares en el API de factual.
http://developer.factual.com/search-place-rank-and-boost/index.html

Categorias de lugares en Factual.
http://developer.factual.com/working-with-categories/